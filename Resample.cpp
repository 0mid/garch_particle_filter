#include "genrand.h"

#include <vector>

using namespace std;

// Resample indices using stratified multinomial sampling as in Pitt 2002,
// Appendix 10-1

void Resample(vector<int> &resampled_indices, const vector<double> &weights,
	      int R) {
  int M = resampled_indices.size();

  double s = 0;
  int j = 0;

  double ordered_uniform = j / (double)M + genrand() / M;

  for (int i = 0; i < R; i++) {
    s += weights[i];

    while (ordered_uniform <= s && j < M) {

      resampled_indices[j] = i;
      j += 1;
      ordered_uniform = j / (double)M + genrand() / M;
    }
  }
}
