#include "normrnd.h"

// definition of normal random number generator
double normrnd() {
  // generate uniform
  double u = genrand();
  // use inverse of normal cdf
  return cndev(u);
}
