#include "ParticleFilter_GARCH.h"

modelparam SetInputParam(const mxArray *inputpar) {
  modelparam par;

  // mean return
  par.mu = mxGetScalar(mxGetField(inputpar, 0, "mu"));

  // measurement noise parameters
  par.delta = mxGetScalar(mxGetField(inputpar, 0, "delta"));

  par.v2 = mxGetScalar(mxGetField(inputpar, 0, "v2"));

  // parameter of innovation distribution
  par.v = mxGetScalar(mxGetField(inputpar, 0, "v"));

  // initial variance
  par.sig20 = mxGetScalar(mxGetField(inputpar, 0, "sig20"));

  // variance parameters
  par.alpha0 = mxGetScalar(mxGetField(inputpar, 0, "alpha0"));
  par.alpha1 = mxGetScalar(mxGetField(inputpar, 0, "alpha1"));
  par.beta1 = mxGetScalar(mxGetField(inputpar, 0, "beta1"));
  par.beta2 = mxGetScalar(mxGetField(inputpar, 0, "beta2"));
  par.beta3 = mxGetScalar(mxGetField(inputpar, 0, "beta3"));
  par.gamma = mxGetScalar(mxGetField(inputpar, 0, "gamma"));

  return par;
}
