#include "ParticleFilter_GARCH.h"

void Get_stats_eval(vector<Particle> &currentparticles, double s, double s_m1,
		    const modelparam &par_eval) {
  // par is the parameter vector used to run the filter,
  // par_eval is the one used to evaluate the Q-fun

  int M = currentparticles.size();

  for (int i = 0; i < M; i++) {
    // old value of sig2
    double oldsig2 = currentparticles[i].sig2_eval;

    double oldsig = sqrt(oldsig2);

    // old value of z_eval
    double oldz = currentparticles[i].z_eval;

    // new value of z_eval
    double newz = par_eval.delta * oldsig * currentparticles[i].eta;

    double epsilon = (s - newz - par_eval.mu - (s_m1 - oldz)) / oldsig;

    // update sig2
    double newsig2 = VarianceUpdate(oldsig2, epsilon, currentparticles[i].eta,
				    currentparticles[i].zeta, par_eval);

    // store updated values
    currentparticles[i].sig2_eval = newsig2;

    currentparticles[i].z_eval = newz;
  }
}
