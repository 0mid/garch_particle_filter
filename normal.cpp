#include "normal.h"

#include <cmath>

// standard normal density function
double ndf(double t) { return 0.398942280401433 * exp(-t * t / 2); }

// standard normal cumulative distribution function
double nc(double x) {
  double result;
  if (x < -7.)
    result = ndf(x) / sqrt(1. + x * x);
  else if (x > 7.)
    result = 1. - nc(-x);
  else {
    result = 0.2316419;
    static double a[5] = { 0.31938153,   -0.356563782, 1.781477937,
			   -1.821255978, 1.330274429 };
    result = 1. / (1 + result * fabs(x));
    result =
	1 -
	ndf(x) *
	    (result *
	     (a[0] +
	      result *
		  (a[1] + result * (a[2] + result * (a[3] + result * a[4])))));
    if (x <= 0.)
      result = 1. - result;
  }
  return result;
}

double cndev(double u) {

  /* returns the inverse of cumulative normal distribution function
  Reference> The Full Monte, by Boris Moro, Union Bank of Switzerland
			  RISK 1995(2)*/

  static double a[4] = { 2.50662823884,  -18.61500062529,
			 41.39119773534, -25.44106049637 };
  static double b[4] = { -8.47351093090,  23.08336743743,
			 -21.06224101826, 3.13082909833 };
  static double c[9] = { 0.3374754822726147, 0.9761690190917186,
			 0.1607979714918209, 0.0276438810333863,
			 0.0038405729373609, 0.0003951896511919,
			 0.0000321767881768, 0.0000002888167364,
			 0.0000003960315187 };
  double x, r;
  x = u - 0.5;
  if (fabs(x) < 0.42) {
    r = x * x;
    r = x * (((a[3] * r + a[2]) * r + a[1]) * r + a[0]) /
	((((b[3] * r + b[2]) * r + b[1]) * r + b[0]) * r + 1.0);
    return (r);
  }
  r = u;
  if (x > 0.0)
    r = 1.0 - u;
  r = log(-log(r));
  r = c[0] +
      r * (c[1] +
	   r * (c[2] +
		r * (c[3] +
		     r * (c[4] +
			  r * (c[5] + r * (c[6] + r * (c[7] + r * c[8])))))));
  if (x < 0.0)
    r = -r;
  return (r);
}
