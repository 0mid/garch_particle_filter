#include "ParticleFilter_GARCH.h"

// sample from x(t) given (y(t),x(t-1)) and put results in currentparticles.x
void GetNewX(vector<Particle> &currentparticles,
	     vector<double> &log_propdensity, modelparam par, double s,
	     double volmultiplier) {
  int M = currentparticles.size();
  double C = 1 / (1 + pow(par.delta, 2));

  for (int i = 0; i < M; i++) {
    double sig2 = currentparticles[i].sig2;
    double sig = sqrt(sig2);

    // double cov_xs = sig2;
    // double v_y    = sig2*(1+par.delta^2);
    // double C      = cov_xs/v_y;

    double mui = par.mu + currentparticles[i].x;
    double E_newx = par.mu + currentparticles[i].x + C * (s - mui);
    // double V_newx  = (sig2-C*cov_xs)*pow(volmultiplier,2);
    double V_newx = sig2 * (1 - C) * pow(volmultiplier, 2);

    currentparticles[i].newx = E_newx + sqrt(V_newx) * normrnd();

    log_propdensity[i] =
	-pow(currentparticles[i].newx - E_newx, 2) / (2 * V_newx) -
	log(V_newx) / 2;
  }
}
