#ifndef PARTICLEFILTER_GARCH_H_INCLUDED
#define PARTICLEFILTER_GARCH_H_INCLUDED

#include "normrnd.h"
#include "mex.h"

#include <vector>
#include <cmath>

using namespace std;

const double PI = 3.14159265358979;
const double BigNegative = -1e+20;

const double log2pi = log(2 * PI);

struct modelparam {
  // initial variance
  double sig20;

  // variance parameters
  double alpha0;
  double alpha1;
  double beta1;
  double beta2;
  double beta3;
  double gamma;

  // mean return
  double mu;

  // measurement noise parameters
  double delta;

  double v2; // GED shape parameter of noise distribution

  // parameter of innovation distribution
  double v; // GED shape parameter of innovation distribution
};

struct Particle {
  // efficient price
  double x;
  double newx;

  // shocks
  double eta;
  double epsilon;
  double zeta;

  // volatility at parameters of the filter
  double sig2;

  // quantities to be propagated at par_eval for complete data likelihood
  // evaluation
  double sig2_eval;
  double z_eval;
};

double min(double a, double b);

double max(double a, double b);

double max(vector<double> &vec, int M);

double gamma(double x);

double lgamma(double x);

double GED_logpdf(double z, double lambda, double GED_const, double v);

void Resample(vector<int> &resampled_indices, const vector<double> &weights,
	      int R);

void Shift_lags(vector<vector<Particle> > &store_lags_new,
		const vector<vector<Particle> > &store_lags,
		const vector<Particle> &currentparticles,
		const vector<int> &resampled_indices, int lags, int t);

double Get_N_eff(const vector<double> &weights);

void GetNewX(vector<Particle> &currentparticles,
	     vector<double> &log_propdensity, modelparam par, double s,
	     double volmultiplier);

void GetModelDensity(vector<double> &log_modeldensity,
		     vector<Particle> &currentparticles, modelparam par,
		     double s);

double VarianceUpdate(double sigma2old, double epsilon, double eta, double zeta,
		      modelparam par);

void UpdateParticles(vector<Particle> &currentparticles, modelparam par);

void Get_stats_eval(vector<Particle> &currentparticles, double s, double s_m1,
		    const modelparam &par_eval);

void GetSuffStats(int ttarget, const double *const s,
		  const modelparam &par_eval,
		  const vector<vector<Particle> > &store_lags, int lag0,
		  mxArray *SuffStats);

void ParticleFilter_GARCH_core(int M, int N_max, int ESS_min, modelparam &par,
			       modelparam &par_eval, unsigned long seed, int T,
			       const double *const s, double *L, int lags,
			       mxArray *SuffStats, double volmultiplier);

modelparam SetInputParam(const mxArray *inputpar);

#endif // PARTICLEFILTER_GARCH_H_INCLUDED
