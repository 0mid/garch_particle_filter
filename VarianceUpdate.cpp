#include "ParticleFilter_GARCH.h"

double VarianceUpdate(double sigma2old, double epsilon, double eta, double zeta,
		      modelparam par) {
  double sigma2new;

  double innov2 =
      par.beta1 * (pow(epsilon - par.gamma, 2) - (1 + pow(par.gamma, 2))) +
      par.beta2 * (pow(eta, 2) - 1) + par.beta3 * (pow(zeta, 2) - 1);

  sigma2new = par.alpha0 * (1 - par.alpha1) + par.alpha1 * sigma2old +
	      sigma2old * innov2;

  return sigma2new;
}
