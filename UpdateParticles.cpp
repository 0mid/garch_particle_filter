#include "ParticleFilter_GARCH.h"
#include "mex.h"

// Update particles:  1.) Set new value of sig2 2.) Set x to newx.
void UpdateParticles(vector<Particle> &currentparticles, modelparam par) {
  int M = currentparticles.size();

  for (int i = 0; i < M; i++) {
    currentparticles[i].zeta = normrnd();

    currentparticles[i].sig2 =
	VarianceUpdate(currentparticles[i].sig2, currentparticles[i].epsilon,
		       currentparticles[i].eta, currentparticles[i].zeta, par);

    currentparticles[i].x = currentparticles[i].newx;
  }
}
