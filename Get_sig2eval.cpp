#include "ParticleFilter_GARCH.h"

void Get_sig2eval(vector<Particle> &currentparticles, double currents,
		  double olds, const modelparam &par,
		  const modelparam &par_eval) {
  // par is the parameter vector used to run the filter,
  // par_eval is the one used to evaluate the Q-fun

  int M = currentparticles.size();

  double currenterror, olderror, currentx, oldx, epsilon;

  for (int i = 0; i < M; i++) {

    currenterror = (currents - currentparticles[i].newx) / par.delta;
    currentx = currents - par_eval.delta * currenterror;

    olderror = (olds - currentparticles[i].x) / par.delta;
    oldx = olds - par_eval.delta * olderror;

    epsilon =
	(currentx - par_eval.mu - oldx) / sqrt(currentparticles[i].sig2_eval);

    currentparticles[i].sig2_eval =
	VarianceUpdate(currentparticles[i].sig2_eval, epsilon, par_eval);
  }
}
