#ifndef GENRAND_H_INCLUDED
#define GENRAND_H_INCLUDED

void sgenrand(unsigned long seed);

double /* generating reals */
/* unsigned long */ /* for integer generation */
genrand();

#endif // GENRAND_H_INCLUDED
