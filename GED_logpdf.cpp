#include "ParticleFilter_GARCH.h"

double GED_logpdf(double z, double lambda, double GED_const, double v) {

  double lnf = GED_const - 0.5 * pow(fabs(z / lambda), v);

  return lnf;
}
