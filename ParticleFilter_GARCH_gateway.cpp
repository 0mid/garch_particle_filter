#include "mex.h"
#include "ParticleFilter_GARCH.h"

// Usage: [L,SuffStats] = ...
// ParticleFilter_GARCH(par,par_eval,s,M,N_max,ESS_min,seed,lags,volmultiplier)
/******************************************************************************
*
* Particle Filter algorithm for the GARCH model
*
* Inputs:
*  par:			model parameter used for filtering
*  par_eval:		model parameter used for complete data loglikelihood
*			evaluations
*  s:			(T+1)*1 pointer of observations (log prices)
*  M:			# of particles at one sampling batch
*  N_max:		how many times sampling is repeated at the maximum
*  ESS_min:		target ESS
*  seed:		random number seed
*  lags:		number of lags for smoothing
*  volmultiplier	multiplier of volatility for proposal distribution
*			(to account for nonnormality of target)
*
*
* Outputs:
*  L:			T*1 pointer to loglikelihoods
*  SuffStats:		T*1 T*1 mxArray for complete data loglikelihood
*
**********************************************************************************/
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  // deal with inputs

  modelparam par = SetInputParam(prhs[0]);

  modelparam par_eval = SetInputParam(prhs[1]);

  double *s = mxGetPr(prhs[2]);

  int T = mxGetM(prhs[2]) - 1;

  int M = mxGetScalar(prhs[3]);

  int N_max = mxGetScalar(prhs[4]);

  int ESS_min = mxGetScalar(prhs[5]);

  int seed = mxGetScalar(prhs[6]);

  int lags = mxGetScalar(prhs[7]);

  double volmultiplier = mxGetScalar(prhs[8]);

  // deal with output

  plhs[0] = mxCreateDoubleMatrix(T, 1, mxREAL);
  double *L = mxGetPr(plhs[0]);

  const char *fnames[] = { "ESS",        "M_effective", "E_x",       "E_sig2",
			   "E_Qepsilon", "E_Qeta",      "E_epsilon", "E_eta" };

  plhs[1] = mxCreateStructMatrix(T, 1, 8, fnames);

  mxArray *SuffStats = plhs[1];

  mxArray *field_value;

  for (int t = 0; t < T; t++) {
    for (int i_field = 0; i_field < 8; i_field++) {
      field_value = mxCreateDoubleMatrix(1, 1, mxREAL);

      mxSetFieldByNumber(SuffStats, t, i_field, field_value);
    }
  }

  // call C routine
  ParticleFilter_GARCH_core(M, N_max, ESS_min, par, par_eval, seed, T, s, L,
			    lags, SuffStats, volmultiplier);
}
