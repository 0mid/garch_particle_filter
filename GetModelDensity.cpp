#include "ParticleFilter_GARCH.h"
#include "mex.h"
#include "normal.h"

void GetModelDensity(vector<double> &log_modeldensity,
		     vector<Particle> &currentparticles, modelparam par,
		     double s) {
  int M = currentparticles.size();

  double eta, lnfeta;

  double epsilon, lnfepsilon;

  // compute some constants used in the density evaluations
  double lambda =
      sqrt(pow(2, -2 / par.v) * gamma(1 / par.v) / gamma(3 / par.v));

  double GED_const =
      log(par.v) - log(lambda) - (1 + 1 / par.v) * log(2.0) - lgamma(1 / par.v);

  double lambda_eta =
      sqrt(pow(2, -2 / par.v2) * gamma(1 / par.v2) / gamma(3 / par.v2));

  double GED_const_eta = log(par.v2) - log(lambda_eta) -
			 (1 + 1 / par.v2) * log(2.0) - lgamma(1 / par.v2);

  // double studentt_constant =
  //   lgamma((par.p+1)/2.0)-0.5*log(PI*par.p)-lgamma(par.p/2) ;

  for (int i = 0; i < M; i++) {
    double sig2 = currentparticles[i].sig2;

    double sig = sqrt(sig2);

    // importance weights due to the discordance in transition density
    epsilon = (currentparticles[i].newx - currentparticles[i].x - par.mu) / sig;

    lnfepsilon = GED_logpdf(epsilon, lambda, GED_const, par.v) - log(sig2) / 2;

    // importance weights due to the discordance in measurement density
    double newz = s - currentparticles[i].newx;

    double sig_newz = par.delta * sig;

    eta = newz / sig_newz;

    // lnfeta = studentt_constant-(par.p+1)/2*log(1+eta*eta/par.p);

    lnfeta = GED_logpdf(eta, lambda_eta, GED_const_eta, par.v2) - log(sig_newz);

    log_modeldensity[i] = lnfepsilon + lnfeta;

    currentparticles[i].epsilon = epsilon;
    currentparticles[i].eta = eta;
  }
}
