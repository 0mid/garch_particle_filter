#include "ParticleFilter_GARCH.h"
#include "float.h"

double min(double a, double b) {
  if (a <= b)
    return a;
  else
    return b;
}

double max(double a, double b) {
  if (a >= b)
    return a;
  else
    return b;
}

// compute maximum of a vector
double max(vector<double> &vec, int M) {
  double maxvec = vec[0];

  for (int i = 1; i < M; i++) {
    if (finite(vec[i]))
      maxvec = max(maxvec, vec[i]);
  }

  return maxvec;
}
