#include "ParticleFilter_GARCH.h"

double Get_N_eff(const vector<double> &weights) {
  int M = weights.size();

  double w2 = 0.0;

  for (int i = 0; i < M; i++)
    w2 += weights[i] * weights[i];

  return 1 / w2;
}
