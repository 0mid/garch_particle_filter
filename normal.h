#ifndef NORMAL_H_INCLUDED
#define NORMAL_H_INCLUDED

// standard normal density function
double ndf(double t);
// standard normal cumulative distribution function
double nc(double x);
//inverse of cumulative normal distribution function
double cndev(double u);

#endif // NORMAL_H_INCLUDED
