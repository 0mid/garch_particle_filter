#include "ParticleFilter_GARCH.h"

void GetSuffStats(int ttarget, const double *const s,
		  const modelparam &par_eval,
		  const vector<vector<Particle> > &store_lags, int lag0,
		  mxArray *SuffStats) {
  double E_x = 0.0;
  double E_sig2 = 0.0;
  double E_epsilon = 0.0;
  double E_eta = 0.0;
  double E_Qepsilon = 0.0;
  double E_Qeta = 0.0;

  double s_current = s[ttarget + 1];
  double s_old = s[ttarget];

  // compute some constants used in the density evaluations
  double lambda = sqrt(pow(2, -2 / par_eval.v) * gamma(1 / par_eval.v) /
		       gamma(3 / par_eval.v));

  double GED_const = log(par_eval.v) - log(lambda) -
		     (1 + 1 / par_eval.v) * log(2.0) - lgamma(1 / par_eval.v);

  double lambda_eta = sqrt(pow(2, -2 / par_eval.v2) * gamma(1 / par_eval.v2) /
			   gamma(3 / par_eval.v2));

  double GED_const_eta = log(par_eval.v2) - log(lambda_eta) -
			 (1 + 1 / par_eval.v2) * log(2.0) -
			 lgamma(1 / par_eval.v2);

  // double
  // studentt_constant=lgamma((par_eval.p+1)/2.0)-0.5*log(PI*par_eval.p)-lgamma(par_eval.p/2)
  // ;

  int M = store_lags.size();

  for (int i = 0; i < M; i++) {
    // calculations to evaluate the complete data loglikelihood
    double sig_eval = sqrt(store_lags[i][lag0].sig2_eval);
    double z_eval_m1 = store_lags[i][lag0].z_eval;
    double z_eval = store_lags[i][lag0 + 1].z_eval;

    double eta = store_lags[i][lag0 + 1].eta;

    double E_s_eval = par_eval.mu + s_old - z_eval_m1 + z_eval;

    // Two parts of the loglikelihood

    // measurement noise part
    // E_Qeta+=(studentt_constant-(par_eval.p+1)/2*log(1+eta*eta/par_eval.p));
    E_Qeta += GED_logpdf(eta, lambda_eta, GED_const_eta, par_eval.v2);

    // system noise part
    double epsilon = (s_current - E_s_eval) / sig_eval;

    E_Qepsilon +=
	GED_logpdf(epsilon, lambda, GED_const, par_eval.v) - log(sig_eval);

    // calculations for monitoring only
    E_x += store_lags[i][lag0 + 1].x;
    E_sig2 += store_lags[i][lag0 + 1].sig2;
    E_epsilon += epsilon;
    E_eta += eta;
  }

  E_x /= M;
  E_sig2 /= M;
  E_epsilon /= M;
  E_eta /= M;
  E_Qeta /= M;
  E_Qepsilon /= M;

  *mxGetPr(mxGetField(SuffStats, ttarget, "E_x")) = E_x;

  *mxGetPr(mxGetField(SuffStats, ttarget, "E_sig2")) = E_sig2;

  *mxGetPr(mxGetField(SuffStats, ttarget, "E_Qepsilon")) = E_Qepsilon;

  *mxGetPr(mxGetField(SuffStats, ttarget, "E_Qeta")) = E_Qeta;

  *mxGetPr(mxGetField(SuffStats, ttarget, "E_epsilon")) = E_epsilon;

  *mxGetPr(mxGetField(SuffStats, ttarget, "E_eta")) = E_eta;
}
