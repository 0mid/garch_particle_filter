#include "ParticleFilter_GARCH.h"
#include "mex.h"
#include "float.h"

void ParticleFilter_GARCH_core(int M, int N_max, int ESS_min, modelparam &par,
			       modelparam &par_eval, unsigned long seed, int T,
			       const double *const s, double *L, int lags,
			       mxArray *SuffStats, double volmultiplier) {

  // set random number seeds
  sgenrand(seed);

  // create M*(L+2) matrix to store fixed-lag particles
  vector<vector<Particle> > store_lags(M, vector<Particle>(lags + 2));

  vector<vector<Particle> > store_lags_new(M, vector<Particle>(lags + 2));

  // initialize state variable
  vector<Particle> currentparticles(M);

  for (int i = 0; i < M; i++) {

    currentparticles[i].x = s[0];
    currentparticles[i].sig2 = par.sig20;
    currentparticles[i].sig2_eval = par_eval.sig20;
    currentparticles[i].z_eval = 0;

    store_lags[i][0] = currentparticles[i];
  }

  // M-vector containing proposal particles
  vector<Particle> proposal(M);

  // M-vector for weights
  vector<double> log_propdensity(M);

  vector<double> log_modeldensity(M);

  vector<Particle> proposal_store(M * N_max);

  vector<double> weights_store(M * N_max);

  vector<double> logweights_store(M * N_max);

  // vector containing resampled indices
  vector<int> resampled_indices(M);

  int t = 0;

  // run time-loop
  while (t < T) {
    // Keep filling up proposal_mtx as long as ESS_min is reached or N_max
    int i_M = 0;

    double sumW = 0.0;
    double sumW2 = 0.0;
    double ESS = 0.0;

    while (ESS < ESS_min && i_M < N_max) {
      for (int i = 0; i < M; i++)
	proposal[i] = currentparticles[i];

      // sample from new value of x
      GetNewX(proposal, log_propdensity, par, s[t + 1], volmultiplier);

      GetModelDensity(log_modeldensity, proposal, par, s[t + 1]);

      for (int i = 0; i < M; i++) {
	int store_i = M * i_M + i;

	logweights_store[store_i] = log_modeldensity[i] - log_propdensity[i];

	proposal_store[store_i] = proposal[i];

	weights_store[store_i] = exp(logweights_store[store_i]);

	if (!finite(weights_store[store_i]))
	  weights_store[store_i] = 0.0;

	sumW += weights_store[store_i];
	sumW2 += weights_store[store_i] * weights_store[store_i];
      }

      ESS = sumW * sumW / sumW2;

      i_M++;

    } // end of sub-cycle generating the new particles

    // compute loglikelihood and normalize weights

    // compute loglikelihood and normalize particles
    int M_effective = i_M * M;

    // mexPrintf("%f \t %d \t %f \n",ESS,M_effective,sumW);

    if (sumW > 0) {
      L[t] = log(sumW / M_effective);

      // normalize weights
      for (int i = 0; i < M_effective; i++)
	weights_store[i] /= sumW;
    } else // normalize with the largest logweight
    {
      // renormalize with maximum weight
      double maxlogweight = max(logweights_store, M_effective);

      sumW = 0.0;
      sumW2 = 0.0;

      for (int i = 0; i < M_effective; i++) {
	weights_store[i] = exp(logweights_store[i] - maxlogweight);

	if (!finite(weights_store[i]))
	  weights_store[i] = 0.0;

	sumW += weights_store[i];
	sumW2 += weights_store[i] * weights_store[i];
      }
      ESS = sumW * sumW / sumW2;

      L[t] = log(sumW / M_effective) + maxlogweight;

      // normalize weights
      for (int i = 0; i < M_effective; i++)
	weights_store[i] /= sumW;
    }

    if (sumW <= 0)
      mexErrMsgTxt("sum of weights is zero in ParticleFilter_jumps.dll!!!");

    *mxGetPr(mxGetField(SuffStats, t, "ESS")) = ESS;

    *mxGetPr(mxGetField(SuffStats, t, "M_effective")) = M_effective;

    // resample particles
    Resample(resampled_indices, weights_store, M_effective);

    for (int i = 0; i < M; i++)
      currentparticles[i] = proposal_store[resampled_indices[i]];

    // update particles:  1.) set new value of sig2 2.) set x to newx,

    UpdateParticles(currentparticles, par);

    // set new value of sig2_eval,z_eval
    Get_stats_eval(currentparticles, s[t + 1], s[t], par_eval);

    // resample past particle paths according to resampled_indices
    // assign new values to last element of particle paths
    Shift_lags(store_lags_new, store_lags, currentparticles, resampled_indices,
	       lags, t);

    store_lags = store_lags_new;

    // save sufficient statistics

    if (t >= lags) {

      GetSuffStats(t - lags, s, par_eval, store_lags, 0, SuffStats);
    }

    t++;
  }

  // now save sufficient stats in the last lag periods
  if (lags > 0) {
    for (int j = 1; j <= lags; j++) {

      GetSuffStats(t - lags + j - 1, s, par_eval, store_lags, j, SuffStats);
    }
  }
}
