//header file for normal rnd generator
#ifndef NORMRND_H_INCLUDED
#define NORMRND_H_INCLUDED

#include "genrand.h"
#include "normal.h"

double normrnd();

#endif // NORMRND_H_INCLUDED
