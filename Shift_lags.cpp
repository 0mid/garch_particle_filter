#include "ParticleFilter_GARCH.h"
#include "mex.h"

void Shift_lags(vector<vector<Particle> > &store_lags_new,
		const vector<vector<Particle> > &store_lags,
		const vector<Particle> &currentparticles,
		const vector<int> &resampled_indices, int lags, int t) {
  int M = currentparticles.size();

  int pathindex;

  if (t <= lags) {
    for (int i = 0; i < M; i++) {

      pathindex = resampled_indices[i] - (resampled_indices[i] / M) * M;

      // resample past particle paths
      for (int j = 0; j <= t; j++)
	store_lags_new[i][j] = store_lags[pathindex][j];

      store_lags_new[i][t + 1] = currentparticles[i];
    }

  } else {
    for (int i = 0; i < M; i++) {
      pathindex = resampled_indices[i] - (resampled_indices[i] / M) * M;
      // resample past particle paths

      for (int j = 0; j <= lags; j++)
	store_lags_new[i][j] = store_lags[pathindex][j + 1];

      store_lags_new[i][lags + 1] = currentparticles[i];
    }
  }
}
